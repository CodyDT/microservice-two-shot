from django.urls import path
from shoes_rest.views import list_shoes, show_shoe


urlpatterns = [
    path("", list_shoes, name="list_shoes"),
    path("<int:id>", show_shoe, name="show_shoe"),
]
