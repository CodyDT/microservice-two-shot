from django.contrib import admin
from shoes_rest.models import Shoe, BinVO

# Register your models here.

@admin.register(BinVO)
class BinVOAdmin(admin.ModelAdmin):
    list_display = (
        "import_href",
        "name",
    )

@admin.register(Shoe)
class ShoeAdmin(admin.ModelAdmin):
    list_display = (
        "model_name",
        "color",
        # "bin",
    )
