import React from 'react';

class ShoeForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      model_name: '',
      color: '',
      picture_url: '',
      bin: '',
      bins:[],
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChangeModelName = this.handleChangeModelName.bind(this);
    this.handleChangeColor = this.handleChangeColor.bind(this);
    this.handleChangePictureUrl = this.handleChangePictureUrl.bind(this);
    this.handleChangeBin = this.handleChangeBin.bind(this);
  }

  async componentDidMount() {
    const URL = 'http://localhost:8100/api/bins/';

    const response = await fetch(URL);

    if (response.ok) {
      const data = await response.json();
      this.setState({ bins: data.bins })
    }
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    delete data.bins;

    const SHOE_URL = 'http://localhost:8080/shoes/'
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(SHOE_URL, fetchConfig);
    if (response.ok) {
      const newShoe = await response.json();
      console.log(newShoe);
      this.setState({
        model_name: '',
        color: '',
        picture_url: '',
        bin: '',
      });
    }
  }

  handleChangeModelName(event) {
    const value = event.target.value;
    this.setState({ model_name: value });
  }
  handleChangeColor(event) {
    const value = event.target.value;
    this.setState({ color: value });
  }
  handleChangePictureUrl(event) {
    const value = event.target.value;
    this.setState({ picture_url: value });
  }
  handleChangeBin(event) {
    const value = event.target.value;
    this.setState({ bin: value });
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new shoe</h1>
            <form onSubmit={this.handleSubmit} id="create-shoe-form">
              <div className="form-floating mb-3">
                <input onChange={this.handleChangeModelName} placeholder="Model name" required type="text" name="model_name" id="model_name" className="form-control" />
                <label htmlFor="model_name">Model name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChangeColor} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChangePictureUrl} placeholder="Picture url" required type="url" name="picture_url" id="picture_url" className="form-control" />
                <label htmlFor="picture_url">Picture url</label>
              </div>
              <div className="mb-3">
                <select onChange={this.handleChangeBin} required name="bin" id="bin" className="form-select">
                  <option value="">Choose a bin</option>
                  {this.state.bins.map(bin => {
                    return (
                      <option key={bin.id} value={bin.href}>{bin.closet_name}</option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default ShoeForm;
