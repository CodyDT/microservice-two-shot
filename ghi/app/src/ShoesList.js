import React from 'react';

function Delete(props) {
  const handleClick = () => {
    console.log(props);
    fetch('http://localhost:8080/shoes/' + props.id, {
      method: 'DELETE',
    })
    .then(() => {
      window.location.reload(false);
    });
  }

  return (
    <button onClick={handleClick}>
      Delete
    </button>
  );
}

function ShoesList(props) {
    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Image</th>
            <th>Model Name</th>
            <th>Color</th>
            <th>Bin</th>
          </tr>
        </thead>
        <tbody>
          {props.shoes.map(shoe => {
            return (
              <tr key={ shoe.id }>
                <td><img src={ shoe.picture_url } alt="shoe" width="400" height="400"/></td>
                <td>{ shoe.model_name }</td>
                <td>{ shoe.color }</td>
                <td>{ shoe.bin.name }</td>
                <td><Delete {... shoe}/></td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }

  export default ShoesList;
